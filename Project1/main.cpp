#include "examples_filters.h"
#include "vulkan_helpers.hpp"

auto main(int argc, char* argv[])-> int {
	const auto width = 10000;
	const auto height = 10000;
	const auto a = 2.0f; 

	/*auto y = std::vector<float>(width * height, 0.71f);
	auto x = std::vector<float>(width * height, 0.65f);*/

	ExampleFilter f("comp.spv");
	auto d_y = vuh::Array<float>::fromHost(std::vector<float>(width * height, 0.71f), f.device, f.physDevice);
	auto d_x = vuh::Array<float>::fromHost(std::vector<float>(width * height, 0.65f), f.device, f.physDevice);
	

	f(d_y, d_x, { width, height, a });

	auto out_tst = std::vector<float>{};
	d_y.to_host(out_tst); // and now out_tst should contain the result (y = y + ax)
	
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(f.physDevice, &deviceProperties);
	 
	return 0;
}