#include <iostream>
#include <vector>

using namespace std;

void func(vector<float> x, vector<float> y)
{
	vector<float> _y;
	_y.resize(y.size());
	for (int i = 0; i < y.size(); i++)
	{
		_y[i] = y[i] + x[i];
	}

	y = _y;
}

void main()
{
	auto y = std::vector<float>(30000, 0.71f);
	auto x = std::vector<float>(30000, 0.65f);

	func(x,y);
}